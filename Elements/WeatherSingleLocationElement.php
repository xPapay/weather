<?php

namespace Modules\Weather\Elements;

use App\Abstracts\ElementType;
use Illuminate\Support\Facades\View;

class WeatherSingleLocationElement extends ElementType
{
	protected static array $fieldsRules = [
        'city' => [
            'validation' => 'required|string',
        ]
    ];

	protected $persists = [
        'city',
    ];

    /**
     * Get the frontend template of the current element
     * Returns a "View"  from View::make(...)
     * @return mixed
     */
    public function view()
    {
		$lang = request()->route('lang', 'en');
		$city = json_decode($this->ce['ce_storage'])->city;
		return View::make('weather::single.weatherWidget', compact('city', 'lang'));

		// this does not work! Why?
        // return $this->renderView(
        //     "weather::single.weatherWidget"
        // );
    }

	/**
     * get the backend editor
     *
     * @return string
     */
    public function editor(): string
    {
        return $this->renderView(
            'weather::single.settings'
        );
    }
}
