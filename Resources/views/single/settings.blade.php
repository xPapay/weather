<div class="row">
    <div class="col-md-4">
        {!!FF::text('city', trans('weather::admin.fields.city.label'), $content['city'], ['required' => true])!!}
    </div>
</div>
