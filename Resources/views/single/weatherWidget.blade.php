<single-location-weather city="{{ $city }}" lang="{{ $lang }}"></single-location-weather>

<script type="module">
    import SingleLocationWeather from '/assets/modules/weather/js/singleLocation.es.js';
    customElements.define('single-location-weather', SingleLocationWeather);
</script>
