const mix = require('laravel-mix');
const path = require('path');
const pkg = require('./package.json');

const publicDir = path.resolve(__dirname, "../../../public");
const packageName = pkg.name;
const packageDir = __dirname;

mix.copy(`${packageDir}/node_modules/@nomad/nd-ui-library-weather/dist/singleLocation.es.js`, `${publicDir}/assets/modules/${packageName}/js/singleLocation.es.js`);
