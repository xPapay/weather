<?php

namespace ExtModules\Weather\Http\Controllers;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class WeatherController extends Controller
{
    public function show(Request $request)
    {
        ['q' => $location] = $request->validate(['q' => ['required']]);

        $appid = config('module.weather.open_weather_api_key');
        $lang = request('lang', 'en');

        if (!$appid) {
            throw new Exception("Please configure 'OPEN_WEATHER_API_KEY' key");
        }

        $url = "http://api.openweathermap.org/data/2.5/weather?q={$location}&APPID=$appid&lang=$lang";
        $tenMinutes = 1 * 60 * 10;
        
        ['status' => $status, 'data' => $data] = Cache::remember($url, $tenMinutes, function() use ($url) {
            $response = Http::get($url);
            return ['status' => $response->status(), 'data' => $response->json()];
        });

        return response()->json(
            $data, 
            $status,
            ['Access-Control-Allow-Origin' => '*']
        );
    }

    public function index()
    {
		// TODO: get(lat), get(long), get(radius), get(limit) (n), fetch n locations in that radius, fetch their weather
		// for better caching, drop gps precsion and round it (less unique requests)
        return ['London', 'Berlin'];
    }
}
