<?php

use Illuminate\Support\Facades\Route;
use ExtModules\Weather\Http\Controllers\WeatherController;

Route::group(['prefix' => 'api/weather', 'as' => 'api.weather.'], function () {
	Route::get('', [WeatherController::class, 'show'])->name('show');
	Route::get('around', [WeatherController::class, 'index'])->name('around');
});
